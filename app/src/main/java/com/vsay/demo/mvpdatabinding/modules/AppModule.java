package com.vsay.demo.mvpdatabinding.modules;

import android.app.Application;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * Created by vsaya on 2/12/17.
 */

@Module
public class AppModule {
  Application mApplication;

  public AppModule(Application mApplication) {
    this.mApplication = mApplication;
  }

  @Provides
  @Singleton
  Application provideApplication() {
    return mApplication;
  }
}
